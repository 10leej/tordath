[Nations](./nations/index.org[)

[Religions and Deities](./religion/index.org)  
[Organizations and Factions](./organizations/index.org)  
[Player Options](./players/index.org)

# The Tor'Dath Campaign Setting

Hi there, this is Tor'Dath, a campaign setting created by Joshua Lee and his players, this is our campaign setting that we play and operate in. The links above take you the many various pages and such that comprise this setting.

![world map](~/Git/tordath/content/tordath.png)
